use crate::to_markdown::ToMarkdown;
use askama::Template;
use chrono::DateTime;
use chrono::Utc;
use mailparse::MailHeaderMap;
use mailparse::MailParseError;
use mailparse::ParsedMail;
use pandoc;
use pandoc::Pandoc;
use serde::Deserialize;
use serde::Serialize;
use slug::slugify;
use std::collections::HashSet;
use std::fmt::Display;
use std::fs;
use std::path::PathBuf;
use yaml_front_matter::{Document, YamlFrontMatter};

pub struct Note {
    pub document: Document<Metadata>,
    // TODO: path
}

#[derive(Deserialize)]
pub struct Metadata {
    pub title: String,
    pub date: DateTime<Utc>,
    pub identifier: Identifier,
    #[serde(default)]
    pub tags: Vec<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Hash, PartialEq, PartialOrd, Ord, Eq)]
#[serde(transparent)]
pub struct Identifier {
    value: String,
}

impl From<&DateTime<Utc>> for Identifier {
    fn from(date: &DateTime<Utc>) -> Self {
        date.format("%Y%m%dT%H%M%S").to_string().into()
    }
}

impl From<String> for Identifier {
    fn from(value: String) -> Self {
        Identifier { value }
    }
}

impl Display for Identifier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.value)
    }
}

impl Note {
    // TODO: Return a result?
    pub fn from_email_message(message: &ParsedMail) -> Self {
        let date = message
            .headers
            .get_first_value("Date")
            .expect("No date in the message header");

        let date = chrono::DateTime::parse_from_rfc2822(&date)
            .expect("Failed to parse the date {date} (expected RFC2822 format)")
            .with_timezone(&Utc);

        let content = message.to_markdown();

        let metadata = Metadata {
            title: message.headers.get_first_value("Subject").unwrap(),
            date,
            tags: vec![],
            identifier: Identifier::from(&date),
        };

        let document = Document { metadata, content };

        Self { document }
    }

    pub fn identifier(&self) -> Identifier {
        self.document.metadata.identifier.clone()
    }

    pub fn title(&self) -> String {
        self.document.metadata.title.clone()
    }

    pub fn tags(&self) -> HashSet<String> {
        HashSet::from_iter(self.document.metadata.tags.clone())
    }

    pub fn date(&self) -> DateTime<Utc> {
        self.document.metadata.date.clone()
    }

    pub fn body(&self) -> String {
        self.document.content.clone()
    }

    pub fn slug(&self) -> String {
        // TODO: Support tags in file name.
        // NOTE: If tags are present, then after slug there is __ and then tags separated by _.
        format!(
            "{identifier}--{slug}",
            identifier = self.identifier(),
            slug = slugify(&self.title()),
        )
    }

    pub fn filename(&self) -> String {
        self.slug() + ".md"
    }

    pub fn read(path: PathBuf) -> Self {
        let markdown = fs::read_to_string(&path).unwrap();
        let document = YamlFrontMatter::parse::<Metadata>(&markdown)
            .unwrap_or_else(|error| panic!("Error parsing {path}: {error}", path = path.display()));
        Self { document }
    }

    pub fn links(&self) -> impl Iterator<Item = String> + '_ {
        let markdown = &self.document.content;
        let parser = pulldown_cmark::Parser::new(markdown);
        parser.filter_map(|event| match event {
            pulldown_cmark::Event::Start(pulldown_cmark::Tag::Link { dest_url, .. }) => {
                Some(dest_url.into_string())
            }
            _ => None,
        })
    }
}

#[derive(Template)]
#[template(path = "note.md")]
struct MarkdownNoteTemplate {
    title: String,
    date: DateTime<Utc>,
    identifier: Identifier,
    content: String,
}

impl ToMarkdown for Note {
    fn to_markdown(&self) -> String {
        let markdown = MarkdownNoteTemplate {
            title: self.title(),
            date: self.date(),
            identifier: self.identifier(),
            content: self.body(),
            // TODO: Support tags
        };
        markdown
            .render()
            .expect("Failed to render the note as markdown")
    }
}

impl ToMarkdown for ParsedMail<'_> {
    fn to_markdown(&self) -> String {
        let body = get_message_body(&self).expect("Message should have a valid body");
        let mut pandoc = Pandoc::new();

        match body {
            ExtractedBody::Html(html) => {
                pandoc.set_input(pandoc::InputKind::Pipe(html));
                pandoc.set_input_format(pandoc::InputFormat::Html, Vec::new());
            }
            ExtractedBody::Text(text) => {
                pandoc.set_input(pandoc::InputKind::Pipe(text));
                pandoc.set_input_format(
                    pandoc::InputFormat::Markdown,
                    vec![pandoc::MarkdownExtension::HardLineBreaks],
                );
            }
        }
        pandoc.set_output(pandoc::OutputKind::Pipe);
        pandoc.set_output_format(pandoc::OutputFormat::MarkdownGithub, Vec::new());

        // This is a bit awkward. Do I have to do it like that?
        if let pandoc::PandocOutput::ToBuffer(markdown) =
            pandoc.execute().expect("Conversion of content failed")
        {
            markdown
        } else {
            panic!("Unexpected output kind from pandoc")
        }
    }
}

fn get_message_body(message: &ParsedMail<'_>) -> Result<ExtractedBody, MailParseError> {
    for part in message.parts() {
        log::debug!("Found part {}", part.ctype.mimetype);
        if part.ctype.mimetype == "text/html" {
            return part.get_body().map(ExtractedBody::Html);
        }
    }

    log::debug!("No text/html body part. Looking for text/plain...");

    for part in message.parts() {
        log::debug!("Found part {}", part.ctype.mimetype);
        if part.ctype.mimetype == "text/plain" {
            return part.get_body().map(ExtractedBody::Text);
        }
    }

    Err(MailParseError::Generic(
        "Could not find neither text/html nor text/plain body part.",
    ))
}

enum ExtractedBody {
    Html(String),
    Text(String),
}
