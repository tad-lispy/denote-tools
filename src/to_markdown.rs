pub trait ToMarkdown {
    fn to_markdown(&self) -> String;
}
