use crate::note;
use crate::note::Note;
use glob::glob;
use std::collections::HashMap;
use std::collections::HashSet;
use std::path::PathBuf;
use url::Url;

#[derive(Default)]
pub struct Notes {
    pub collection: HashMap<note::Identifier, Note>,
}

impl Notes {
    pub fn new(collection: HashMap<note::Identifier, Note>) -> Self {
        Self { collection }
    }

    pub fn load(directory_path: PathBuf) -> Self {
        let pattern = format!("{path}/*--*.md", path = directory_path.display());
        let note_paths = glob(&pattern).expect("glob pattern should work just fine");
        let mut notes = Notes::default();

        for note_path in note_paths {
            let note_path = note_path.expect("The matching path should be correct");
            log::debug!("Loading {path}", path = note_path.display());
            let note = Note::read(note_path);
            notes.insert(note);
        }

        notes
    }

    pub fn insert(&mut self, note: Note) -> &Self {
        self.collection.insert(note.identifier(), note);
        self
    }

    pub fn list(&self) -> impl Iterator<Item = &Note> + '_ {
        self.collection.values()
    }

    pub fn backlinks(&self) -> Backlinks {
        let mut backlinks = Backlinks::default();

        for source in self.list() {
            for link in source.links() {
                log::debug!("Parsing link {}", link);
                match Url::parse(&link) {
                    Ok(url) => {
                        if url.scheme() == "denote" {
                            let target: note::Identifier = url.path().to_string().into();
                            backlinks
                                .entry(target)
                                .and_modify(|sources| {
                                    sources.insert(source.identifier());
                                })
                                .or_insert(vec![source.identifier()].into_iter().collect());
                        }
                    }
                    Err(error) => {
                        // TODO: Gracefully support email autolinks. No need to
                        // warn about them. Probably best to avoid emitting them
                        // from Note::links method
                        log::warn!(
                            "Problem processing link {link} in note {source_id} ({title}): {error}",
                            source_id = source.identifier(),
                            title = source.title(),
                        );
                    }
                }
            }
        }
        backlinks
    }

    pub fn tags(&self) -> HashMap<String, Vec<&Note>> {
        let mut tags: HashMap<String, Vec<&Note>> = HashMap::default();
        for note in self.list() {
            for tag in note.tags() {
                tags.entry(tag).or_default().push(note);
            }
        }
        tags
    }
}

impl From<Vec<Note>> for Notes {
    fn from(input: Vec<Note>) -> Self {
        let mut collection = HashMap::with_capacity(input.len());
        for note in input {
            collection.insert(note.identifier(), note);
        }
        Self::new(collection)
    }
}

type Backlinks = HashMap<
    note::Identifier,
    // TODO: Store context, i.e. element around the link, like a paragraph
    HashSet<note::Identifier>,
>;
