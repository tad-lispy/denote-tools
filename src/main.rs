mod html_export;
mod imap_import;
mod note;
mod notes;
mod to_markdown;

use crate::html_export::HtmlExport;
use crate::notes::Notes;
use clap;
use clap::Parser;
use env_logger;
use std::fs::create_dir_all;
use std::path::PathBuf;
use std::process::exit;

#[derive(clap::Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Command,
}

#[derive(clap::Subcommand)]
enum Command {
    /// Import notes from an IMAP server
    Import { directory: PathBuf },
    /// List notes
    List { directory: PathBuf },
    /// List tags
    Tags { directory: PathBuf },
    /// Export notes to HTML
    Export { from: PathBuf, into: PathBuf },
}

fn main() {
    env_logger::init();

    let cli = Cli::parse();

    match cli.command {
        Command::Import { directory } => import(directory),
        Command::List { directory } => list_notes(directory),
        Command::Tags { directory } => list_tags(directory),
        Command::Export { from, into } => export(from, into),
    }
}

fn import(directory_path: PathBuf) {
    check_directory(&directory_path);

    let config = imap_import::Config::from_env();

    // NOTE: Avoid leaking secrets in logs
    #[cfg(debug_assertions)]
    log::debug!("Configuration loaded {config:?}");

    imap_import::import(&config, directory_path)
}

fn list_notes(directory_path: PathBuf) {
    check_directory(&directory_path);

    log::debug!("Listing notes");

    for note in Notes::load(directory_path).list() {
        println!("{date}\t{title}", date = note.date(), title = note.title())
    }
}

fn list_tags(directory_path: PathBuf) {
    check_directory(&directory_path);

    log::debug!("Listing tags");

    for (tag, notes) in Notes::load(directory_path).tags() {
        println!("{tag}\t{count}", count = notes.len())
    }
}

fn export(from_directory: PathBuf, into_directory: PathBuf) {
    create_dir_all(&into_directory).expect("ensuring the export directory exists shouldn't fail");
    let notes = Notes::load(from_directory);

    notes.export_html(&into_directory)
}

fn check_directory(directory_path: &PathBuf) {
    if !directory_path.exists() {
        log::error!(
            "Path doesn't exist: {path}",
            path = directory_path.display()
        );
        exit(1)
    }
    if !directory_path.is_dir() {
        log::error!("Not a directory: {path}", path = directory_path.display());
        exit(2)
    }
}
