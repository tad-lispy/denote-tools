use crate::note::Note;
use crate::to_markdown::ToMarkdown;
use glob::glob;
use imap;
use log;
use mailparse::parse_mail;
use native_tls;
use native_tls::TlsStream;
use std::env;
use std::fs;
use std::net::TcpStream;
use std::path::PathBuf;

pub fn import(config: &Config, directory: impl Into<PathBuf>) {
    log::info!("Starting import");

    let import_path = directory.into();

    let mut session = connect(&config);
    log::debug!("Client connected and authenticated");

    let mailboxes = session
        .list(Some(""), Some("*"))
        .expect("Failed to list mailboxes");
    log::debug!("There are {} mailboxes", mailboxes.len());
    for name in &mailboxes {
        log::debug!("- {}", name.name());
    }

    session.select(&config.mailbox).unwrap();
    log::debug!("Inbox selected: {}", &config.mailbox);

    let messages = session.fetch("1:*", "RFC822").unwrap();
    log::debug!("{} messages fetched", messages.len());

    for message in &messages {
        let body = message.body().unwrap();
        let parsed = parse_mail(body).unwrap();
        let note = Note::from_email_message(&parsed);

        let pattern = format!(
            "{export_path}/{identifier}--*",
            export_path = import_path.display(),
            identifier = note.identifier()
        );

        if let Some(existing) = glob(&pattern)
            .expect("glob pattern should work just fine")
            .next()
        {
            let existing =
                existing.expect("it should be possible to extract path of the preexisting note");
            log::info!("Skipping import of '{title}'. There already exists note with the same identifier: {existing}",
                  title = &note.title(),
                  existing = existing.display()
            );
        } else {
            let path = import_path.join(note.filename());
            log::info!(
                "Importing {path} {title}",
                path = path.display(),
                title = note.title()
            );
            fs::write(path, note.to_markdown()).expect("Failed to write the file");
        }
    }

    if log::log_enabled!(log::Level::Debug) {
        log::debug!("Debug logging for IMAP session enabled");
        session.debug = true;
    }

    disconnect(session).unwrap();

    log::info!("Done.");
}

#[derive(Debug)]
pub struct Config {
    host: String,
    port: u16,
    username: String,
    password: String,
    mailbox: String,
    cart_path: Option<String>,
}

impl Config {
    pub fn from_env() -> Self {
        Self {
            host: env::var("imap_host")
                .expect("There should be an environment variable named 'imap_host'"),
            port: env::var("imap_port")
                .expect("There should be an environment variable named 'imap_port'")
                .parse::<u16>()
                .expect("The 'imap_port' environment variable must be a positive number"),
            username: env::var("imap_username")
                .expect("There should be an environment variable named 'imap_username'"),
            password: env::var("imap_password")
                .expect("There should be an environment variable named 'imap_password'"),
            mailbox: env::var("imap_mailbox")
                .expect("There should be an environment variable named 'imap_mailbox'"),
            cart_path: env::var("imap_cert_path")
                .map(Option::Some)
                .or_else(|error| {
                    if error == env::VarError::NotPresent {
                        Ok(None)
                    } else {
                        Err(error)
                    }
                })
                .expect("The value of 'imap_cert_path' environment variable is invalid"),
        }
    }
}

// TODO: Return a result. How to combine different types of errors (io, tls, imap)?
fn connect(config: &Config) -> imap::Session<TlsStream<TcpStream>> {
    // Setup TLS with a certificate from Proton Bridge
    // TODO: Automate certificate export from Proton Mail Bridge, or at least document it.
    // SEE: https://github.com/ProtonMail/proton-bridge/issues/315
    let mut tls = native_tls::TlsConnector::builder();
    if let Some(cert_path) = &config.cart_path {
        log::debug!("Loading root certificate from {}", &cert_path);
        let pem = &fs::read(cert_path).expect(&format!(
            "there should be a PEM certificate for the IMAP server at {cert_path}"
        ));
        let cert = native_tls::Certificate::from_pem(pem).unwrap();
        tls.add_root_certificate(cert);
    }
    let tls = tls.build().unwrap();

    let client =
        imap::connect_starttls((config.host.clone(), config.port), &config.host, &tls).unwrap();
    log::debug!("Client connected");

    client.login(&config.username, &config.password).unwrap()
}

fn disconnect<T>(mut session: imap::Session<T>) -> Result<(), imap::Error>
where
    T: std::io::Write + std::io::Read,
{
    // TODO: Report. See https://github.com/jonhoo/rust-imap/issues/210
    let result = session.logout();
    match &result {
        Ok(_) => result,
        Err(err) => match &err {
            imap::Error::Parse(imap::error::ParseError::Invalid(bytes)) => {
                if bytes.clone() == "* BYE\r\n".as_bytes() {
                    log::debug!("The server said BYE and the client freaked out, but it's fine");
                    Ok(())
                } else {
                    result
                }
            }
            _ => result,
        },
    }
}
