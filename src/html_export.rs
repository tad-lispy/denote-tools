use crate::note;
use crate::{note::Note, notes::Notes};
use askama::Template;
use chrono::{DateTime, Utc};
use itertools::Itertools;
use std::path::PathBuf;
use std::process;
use url::Url;

pub trait HtmlExport {
    fn export_html(&self, directory: &PathBuf);
}

impl HtmlExport for Notes {
    fn export_html(&self, directory: &PathBuf) {
        // TODO: Read from a file or category metadata
        let featured_categories = vec![
            FeaturedCategory {
                icon: "person".into(),
                label: "People".into(),
                name: "person".into(),
            },
            FeaturedCategory {
                icon: "person-digging".into(),
                label: "Work".into(),
                name: "work".into(),
            },
            FeaturedCategory {
                icon: "lightbulb".into(),
                label: "Ideas".into(),
                name: "idea".into(),
            },
            FeaturedCategory {
                icon: "compass-drafting".into(),
                label: "Projects".into(),
                name: "project".into(),
            },
            FeaturedCategory {
                icon: "calendar-day".into(),
                label: "Events".into(),
                name: "event".into(),
            },
            FeaturedCategory {
                icon: "location-dot".into(),
                label: "Places".into(),
                name: "place".into(),
            },
        ];

        let backlinks = self.backlinks();

        for note in self.list() {
            let backlinks: Vec<&Note> = backlinks
                .get(&note.identifier())
                .cloned()
                .unwrap_or_default()
                .into_iter()
                .filter_map(|source| self.collection.get(&source))
                .collect();

            log::debug!(
                "exporting {identifier} {title}",
                identifier = note.identifier(),
                title = note.title()
            );

            let title = note.title();
            let date = note.date();
            let tags: Vec<LinkToTag> = note
                .tags()
                .into_iter()
                .sorted()
                .map(LinkToTag::from)
                .collect();
            let backlinks = backlinks.into_iter().map(LinkToNote::from).collect();

            let mut markdown_options = pulldown_cmark::Options::empty();
            markdown_options.insert(pulldown_cmark::Options::ENABLE_FOOTNOTES);
            markdown_options.insert(pulldown_cmark::Options::ENABLE_SMART_PUNCTUATION);
            markdown_options.insert(pulldown_cmark::Options::ENABLE_STRIKETHROUGH);
            markdown_options.insert(pulldown_cmark::Options::ENABLE_TABLES);
            markdown_options.insert(pulldown_cmark::Options::ENABLE_TASKLISTS);
            // NOTE: To rewrite links I need access to the whole Notes collection. Probably this function needs to be a method of Notes.
            let mut body = String::new();

            let markdown = note.body();
            let rewrite_links = |event| {
                if let pulldown_cmark::Event::Start(pulldown_cmark::Tag::Link {
                    link_type,
                    dest_url,
                    title,
                    ..
                }) = &event
                {
                    match Url::parse(&dest_url) {
                        Ok(url) => {
                            if url.scheme() == "denote" {
                                let identifier: note::Identifier = url.path().to_string().into();
                                if let Some(note) = self.collection.get(&identifier) {
                                    let href = export_path(note);
                                    pulldown_cmark::Event::Start(pulldown_cmark::Tag::Link {
                                        link_type: link_type.clone(),
                                        dest_url: href.into(),
                                        title: title.clone(),
                                        id: "".into(),
                                    })
                                } else {
                                    event
                                }
                            } else {
                                event
                            }
                        }
                        Err(error) => {
                            // TODO: Gracefully support email autolinks. No need to
                            // warn about them. Probably best to avoid emitting them
                            // from Note::links method
                            log::warn!(
                                    "Problem processing link {dest_url} in note {source_id} ({title}): {error}",
                                    source_id = note.identifier(),
                                    title = note.title(),
                                );
                            event
                        }
                    }
                } else {
                    event
                }
            };
            let parser =
                pulldown_cmark::Parser::new_ext(&markdown, markdown_options).map(rewrite_links);

            pulldown_cmark::html::push_html(&mut body, parser);

            let html = NoteTemplate {
                title,
                date,
                tags,
                body,
                backlinks,
            }
            .render()
            .expect("The template should render just fine");

            let export_path = directory.join(export_path(note));

            std::fs::write(export_path, html).expect("Writing to a file should work");
        }

        for (tag, notes) in self.tags() {
            log::info!("exporting tag page {tag}");

            let notes: Vec<LinkToNote> = notes
                .into_iter()
                .sorted_by(|a, b| a.date().cmp(&b.date()))
                .rev()
                .map(LinkToNote::from)
                .collect();

            let href = LinkToTag::from(&tag).href();
            let export_path = directory.join(href);

            let other_tags = self
                .tags()
                .keys()
                .sorted()
                .map(|tag| LinkToTag::from(tag.to_owned()))
                .collect();

            let html = TagTemplate {
                tag,
                featured_categories: featured_categories.clone(),
                notes,
                other_tags,
                date: Utc::now(),
            }
            .render()
            .expect("The tag template to render");

            std::fs::create_dir_all(
                export_path
                    .parent()
                    .expect("The category page path should have a parent directory"),
            )
            .expect("Creating the directory should work");
            std::fs::write(export_path, html).expect("Writing to a file should work");
        }

        // Write categories/index.html
        let html = CategoriesIndexTemplate {
            date: Utc::now(),
            categories: self
                .tags()
                .keys()
                .sorted()
                .map(|tag| LinkToTag::from(tag.to_owned()))
                .collect(),
            featured_categories: featured_categories.clone(),
        }
        .render()
        .expect("The categories-index.html template should render just fine");

        let export_path = directory.join("categories/index.html");
        std::fs::create_dir_all(
            export_path
                .parent()
                .expect("The category page path should have a parent directory"),
        )
        .expect("Creating the directory should work");
        std::fs::write(export_path, html).expect("Writing to a file should work");

        // Write index.html
        let index_notes: Vec<&Note> = self
            .list()
            .filter(|note| note.tags().contains(&String::from("entrypoint")))
            .sorted_by(|a, b| a.date().cmp(&b.date()))
            .rev()
            .collect();

        let html = IndexTemplate {
            date: Utc::now(),
            notes: index_notes.into_iter().map(LinkToNote::from).collect(),
            tags: self
                .tags()
                .keys()
                .sorted()
                .map(|tag| LinkToTag::from(tag.to_owned()))
                .collect(),
            featured_categories: featured_categories.clone(),
        }
        .render()
        .expect("The index.html template should render just fine");

        let export_path = directory.join("index.html");
        std::fs::write(export_path, html).expect("Writing to a file should work");

        // Write all.html
        let all_notes: Vec<&Note> = self
            .list()
            .sorted_by(|a, b| a.date().cmp(&b.date()))
            .rev()
            .collect();

        let html = AllNotesTemplate {
            date: Utc::now(),
            notes: all_notes.into_iter().map(LinkToNote::from).collect(),
            tags: self
                .tags()
                .keys()
                .sorted()
                .map(|tag| LinkToTag::from(tag.to_owned()))
                .collect(),
            featured_categories: featured_categories.clone(),
        }
        .render()
        .expect("The index.html template should render just fine");

        let export_path = directory.join("all.html");
        std::fs::write(export_path, html).expect("Writing to a file should work");

        log::info!("Copy assets from {}", assets_path().display());
        for asset in vec![
            "styles",
            "apple-touch-icon.png",
            "favicon.ico",
            "favicon.svg",
            "favicon-96x96.png",
            "site.webmanifest",
            "tad-notes-192x192.png",
            "tad-notes-512x512.png",
        ] {
            let from = assets_path().join(asset);

            // TODO: Make the copy process cross-platform.
            process::Command::new("cp")
                .arg("-r")
                .arg(from)
                .arg(directory)
                .output()
                .unwrap();
        }
    }
}

#[derive(Template)]
#[template(path = "note.html")]
struct NoteTemplate {
    title: String,
    date: DateTime<Utc>,
    tags: Vec<LinkToTag>,
    backlinks: Vec<LinkToNote>,
    body: String,
}

#[derive(Template)]
#[template(path = "tag.html")]
struct TagTemplate {
    tag: String,
    featured_categories: Vec<FeaturedCategory>,
    notes: Vec<LinkToNote>,
    date: DateTime<Utc>,
    other_tags: Vec<LinkToTag>,
}

#[derive(Template)]
#[template(path = "index.html")]
struct IndexTemplate {
    date: DateTime<Utc>,
    notes: Vec<LinkToNote>,
    tags: Vec<LinkToTag>,
    featured_categories: Vec<FeaturedCategory>,
}

#[derive(Template)]
#[template(path = "all.html")]
struct AllNotesTemplate {
    date: DateTime<Utc>,
    notes: Vec<LinkToNote>,
    tags: Vec<LinkToTag>,
    featured_categories: Vec<FeaturedCategory>,
}

#[derive(Template)]
#[template(path = "categories-index.html")]
struct CategoriesIndexTemplate {
    date: DateTime<Utc>,
    categories: Vec<LinkToTag>,
    featured_categories: Vec<FeaturedCategory>,
}

struct LinkToNote {
    title: String,
    href: String,
    tags: Vec<LinkToTag>,
}

impl From<&Note> for LinkToNote {
    fn from(note: &Note) -> Self {
        Self {
            title: note.title(),
            tags: note.tags().into_iter().map(LinkToTag::from).collect(),
            href: format!("./{}", export_path(note)),
        }
    }
}

// TODO: Rename Tag to Category
/// Used to display a selection of often browsed categories, like "person", "project", "work"
#[derive(Debug, Clone)]
struct FeaturedCategory {
    /// A name of the category (tag), e.g. "project"
    name: String,
    /// A label to display, e.g. "Projects"
    label: String,
    /// A name of the icon in the Font Awesome collection, e.g. "compass-drafting"
    icon: String,
}

struct LinkToTag {
    name: String,
}

impl LinkToTag {
    fn href(&self) -> String {
        format!("categories/{tag}.html", tag = self.name)
    }
}

impl From<String> for LinkToTag {
    fn from(name: String) -> Self {
        LinkToTag { name }
    }
}

impl From<&String> for LinkToTag {
    fn from(name: &String) -> Self {
        LinkToTag {
            name: name.to_owned(),
        }
    }
}

fn export_path(note: &Note) -> String {
    note.slug() + ".html"
}

fn assets_path() -> PathBuf {
    if let Ok(manifest_dir) = std::env::var("CARGO_MANIFEST_DIR") {
        PathBuf::from(manifest_dir).join("templates")
    } else {
        let executable_path =
            std::env::current_exe().expect("Program to determine it's executable path");

        // NOTE: This assumes that the program is distributed using Nix, so there is /nix/store/*/{bin,lib}.
        // If a different package system is to be supported, this needs to get more sophisticated.
        executable_path
            .parent()
            .and_then(|bin| bin.parent())
            .map(|root| root.join("lib"))
            .expect("Assets path")
    }
}
