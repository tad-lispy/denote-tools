{
  description = "Tools Tad uses to make and keep his notes";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        project-name = "tad-notes";
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        buildInputs = with pkgs; [
          pandoc
        ];
        nativeBuildInputs = with pkgs; [
          (rust-bin.fromRustupToolchainFile ./rust-toolchain.toml)
          gcc
          openssl
          pkg-config
          gnumake
          findutils
          gzip
          lld
          rustPlatform.bindgenHook
          cargo-watch
        ];
      in
      {
        devShell = pkgs.mkShell {
          inherit buildInputs nativeBuildInputs;
          name = "${project-name}-develpoment-shell";
          packages = [
            pkgs.rust-analyzer
            pkgs.jq
            pkgs.miniserve
          ];
          project_name = project-name; # Expose as an environment variable for make
        };
        defaultPackage = pkgs.rustPlatform.buildRustPackage {
          name = project-name;
          version = "1.0.0";
          src = ./.;
          cargoLock = {
            lockFile = ./Cargo.lock;
          };
          postInstall = "make install-assets";

          inherit buildInputs nativeBuildInputs;
          PKG_CONFIG_PATH = "${pkgs.openssl.dev}/lib/pkgconfig";
        };
      }
    );
}
