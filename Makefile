include Makefile.d/defaults.mk

all: ## Build the program (DEFAULT)
all: test build
.PHONY: all

build: $(shell find src/)
build: Cargo.lock
build:
	cargo build --release
.PHONY: build

test:
	cargo test
.PHONY: test


install-assets: ## Install the assets in the ${prefix}/lib/ directory
install-assets: prefix ?= $(out)
install-assets:
	test $(prefix)
	mkdir --parents $(prefix)/lib/
	cp --recursive templates/* $(prefix)/lib/
.PHONY: install-assets

### DEVELOPMENT

develop: ## Rebuild and run a development version of the program
develop: log-level ?= info
develop: import list export
.PHONY: develop


develop/export: ## Watch and run export on every change
develop/export: notes ?= notes
develop/export: exported ?= exported
develop/export: log-level ?= info
develop/export:
	RUST_LOG=$(log-level) cargo watch --exec "run export $(notes) $(exported)"
.PHONY: develop/export

import: ## Try importing the notes from an IMAP server
import: notes ?= notes
import:
	RUST_LOG=$(log-level) cargo run -- import $(notes)
.PHONY: import

tags: ## Try listing the tags
tags: notes ?= notes
tags: log-level ?= info
tags:
	RUST_LOG=$(log-level) cargo run -- tags $(notes)
.PHONY: tags

list: ## Try listing the notes
list: notes ?= notes
list: log-level ?= info
list:
	RUST_LOG=$(log-level) cargo run -- list $(notes)
.PHONY: list

export: ## Try exporting the notes
export: notes ?= notes
export: exported ?= exported
export: log-level ?= info
export:
	RUST_LOG=$(log-level) cargo run -- export $(notes) $(exported)
.PHONY: export

serve: ## Serve exported notes
serve: exported ?= exported
serve: log-level ?= info
serve:
	chmod u+w $(exported)/styles/
	rm --recursive --force $(exported)/styles
	ln --symbolic --force ../templates/styles $(exported)/styles

	miniserve --index=index.html --interfaces=127.0.0.1 $(exported)
.PHONY: serve

clean: ## Remove all build artifacts
clean:
	git clean -dfX \
		--exclude='!.envrc.private' \
		--exclude='!private/**/*'
.PHONY: clean

help: ## Print this help message
help:
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help


### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs
.PHONY: result

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)


### OCI BUILD IMAGE RELATED

build-image: ## Create an OCI image with build environment (to be used in CI)
build-image: flake.nix
build-image: flake.lock
build-image: rust-toolchain.toml
build-image:
	nix build \
		--print-build-logs \
		.#docker-image
	cp --dereference result $@

load-build-image: ## Load the OCI image using Podman
load-build-image: build-image
	gzip --decompress $^ --to-stdout | podman image load
.PHONY: load-build-image

build-inside-container: ## Build the program in an OCI container (e.g. to test CI)
build-inside-container: load-build-image
	# We need to know the project name to know which image to load. Can we do better?
	test $${project_name}
	podman run \
		--rm \
		--interactive \
		--tty \
		--volume ${PWD}:/src \
		--workdir /src \
		--entrypoint make \
		localhost/$(project_name)-build-image \
		public
.PHONY: build-inside-container
