---
title:      {{ title|yaml|trim }}
date:       {{ date|yaml|trim }}
identifier: {{ identifier|yaml|trim }}
---

{{ content|trim }}

